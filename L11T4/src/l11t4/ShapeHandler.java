package l11t4;

import java.util.ArrayList;
import l11t4.FXMLDocumentController.Point;
import l11t4.FXMLDocumentController.Viiva;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * Jun 11, 2018
 * Projekti: l11t3
 * Tiedosto: ShapeHandler
 */
public class ShapeHandler {
        static public ShapeHandler s = null;
        public ArrayList<Point> points = new ArrayList();
        public ArrayList<Viiva> lines = new ArrayList();
        
        private ShapeHandler(){
            
        }
        static public ShapeHandler getInstance(){
            if (s == null){
            s = new ShapeHandler();
            }
        
            return s;
        }
    }