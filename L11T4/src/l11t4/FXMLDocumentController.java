/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l11t4;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    public AnchorPane bg;
    public ShapeHandler s = null;
    public double startx = 9999;
    public double endx = 9999;
    public double starty = 9999;
    public double endy = 9999;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void clickAction(MouseEvent event) {
        double d = 9999;
        double ds;
        int k = 0; int j = 0;
        
        try{
            for (Point p:s.points){
                ds = Math.sqrt(Math.pow((event.getX()-p.xc),2)+Math.pow((event.getY()-p.yc),2));
                if (ds<=d){
                    d=ds;
                    j = k;
                }
                k++;
            }
            k = 0;
        }
        catch (NullPointerException ex){}
        
        if(d>=7){
            Point p = new Point(event.getX(),event.getY());
        }
        else if (d<7){
            if (startx == 9999 && endx == 9999){
                startx = s.points.get(j).xc;
                starty = s.points.get(j).yc;
            }
            
            else if(endx == 9999){
                endx = s.points.get(j).xc;
                endy = s.points.get(j).yc;
                Viiva v = new Viiva(startx,starty,endx,endy);
                startx = 9999;
                starty= 9999;
                endx=9999;
                endy=9999;
            }
            
        }
    }

    EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>(){ 
        @Override 
        public void handle(MouseEvent e) { 
            System.out.println("Hei olen piste!");
        }
    };
       
    
    public class Point{
        private double xc;
        private double yc;


        public Point(double x,double y){
            s = ShapeHandler.getInstance();

            xc = x;
            yc = y;
            Circle c = new Circle(xc,yc,7);
            bg.getChildren().add(c);
            s.points.add(this);
            c.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
            
        } 
    }
    
    public class Viiva{
        public double startx;
        public double endx;
        public double starty;
        public double endy;


        public Viiva(double x1,double y1,double x2,double y2){
            s = ShapeHandler.getInstance();
            startx = x1;
            starty = y1;
            endx = x2;
            endy = y2;
            Line line = new Line();
            line.setStrokeWidth(5);
            line.setStartX(x1); 
            line.setStartY(y1); 
            line.setEndX(x2); 
            line.setEndY(y2);
            Group root = new Group(line);
            bg.getChildren().clear();
            for (Point p:s.points){
                Circle c = new Circle(p.xc,p.yc,7);
                bg.getChildren().add(c);
            }
            bg.getChildren().add(root);
            s.lines.add(this);
        } 
    }
}
