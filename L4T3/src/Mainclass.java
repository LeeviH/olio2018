import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * May 29, 2018
 * Projekti: 
 * Tiedosto: Mainclass
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String filenameI = "input.txt";
        String filenameO = "output.txt";
        String text = "";

        ReadAndWriteIO raw = new ReadAndWriteIO(filenameI,filenameO);
        
        try {
            text = raw.readText();
        } catch (IOException ex) {
            Logger.getLogger(Mainclass.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            raw.writeText(text);
        } catch (IOException ex) {
            Logger.getLogger(Mainclass.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }

}
