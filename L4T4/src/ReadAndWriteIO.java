import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * May 29, 2018
 * Projekti: 
 * Tiedosto: ReadAndWriteIO
 */
public class ReadAndWriteIO {
    private String filenameI;
    private String filenameO;
    private String line;
    private String fulltext = "";
    
    public ReadAndWriteIO(String i,String o){
        filenameI = i;
        filenameO = o;
    }
    
    public void writeText(String text) throws IOException{
        BufferedWriter out = new BufferedWriter(new FileWriter(filenameO));
        out.write(text);
        out.close();
    }
    
    public String readText() throws IOException{
        BufferedReader in = new BufferedReader(new FileReader(filenameI));
        line = in.readLine();

        while (line != null) {
            if (line.length() < 30 && line.trim().length() != 0 && line.indexOf("v")>0){
                fulltext += line + "\n";
            }
            line = in.readLine();
        }
        in.close();
        
        return fulltext;
    }
}