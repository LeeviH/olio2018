/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l9t1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button listButton;
    @FXML
    private Button searchButton;
    @FXML
    private ComboBox<?> theaterList;
    @FXML
    private TextField startInput;
    @FXML
    private TextField endIput;
    @FXML
    private TextField dateInput;
    @FXML
    private TextField nameInput;
    @FXML
    private Label outputField;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void listMovies(ActionEvent event) {
    }

    @FXML
    private void searchName(ActionEvent event) {
    }
    
}
