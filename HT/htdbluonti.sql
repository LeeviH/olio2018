.open htdb.db
.headers on
.mode column


CREATE TABLE "smartpost" (
	"id"		INTEGER		PRIMARY KEY,
	"nimi"		VARCHAR(60)	NOT NULL,
	"posticode"	VARCHAR(10)	NOT NULL,
	"city"		VARCHAR(20)	NOT NULL,
	"osoite"	VARCHAR(40)	NOT NULL,
	"lat"		REAL		NOT NULL,
	"long"		REAL		NOT NULL,
	"auki"		VARCHAR(100)	NOT NULL
);

CREATE TABLE "paketti" (
	"pakettiid"	INTEGER		PRIMARY KEY AUTOINCREMENT,
	"esineid"	INTEGER		NOT NULL,
	"luokka"	INTEGER		NOT NULL,
	"koko"		REAL		NOT NULL,
	"painoraja"	REAL		NOT NULL,

	FOREIGN KEY("esineid") REFERENCES "esine"("esineid"),

	CHECK ("luokka" IN (1,2,3)),
	CHECK ("koko" > 0),
	CHECK ("painoraja" > 0)
);

CREATE TABLE "esine" (
	"esineid"	INTEGER		PRIMARY KEY AUTOINCREMENT,
	"nimi"		VARCHAR(30)	NOT NULL,
	"koko"		REAL		NOT NULL,
	"paino"		REAL		NOT NULL,
	"sarkyva"	BOOL		NOT NULL,

	CHECK ("koko" > 0),
	CHECK ("paino" > 0)
);

INSERT INTO "esine" ("nimi","koko","paino","sarkyva")
	VALUES('Elefantti',200,10110,'False');
INSERT INTO "esine" ("nimi","koko","paino","sarkyva")
	VALUES('Paita',0.23,0.2,'False');
INSERT INTO "esine" ("nimi","koko","paino","sarkyva")
	VALUES('Lasi',2,2,'True');
INSERT INTO "esine" ("nimi","koko","paino","sarkyva")
	VALUES('Pallo',5,4,'False');


CREATE TABLE "lahetys" (
	"lahetysid"	INTEGER		PRIMARY KEY AUTOINCREMENT,
	"pakettiid"	INTEGER 	NOT NULL,
	"startid"	INTEGER		NOT NULL,
	"endid"		INTEGER		NOT NULL,

	FOREIGN KEY("pakettiid") REFERENCES "paketti"("pakettiid"),
	FOREIGN KEY("startid") REFERENCES "smartpost"("id"),
	FOREIGN KEY("endid") REFERENCES "smartpost"("id")
);

CREATE TABLE "logitiedot" (
	"logiid"	INTEGER		PRIMARY KEY AUTOINCREMENT,
	"aika"		VARCHAR(15)	NOT NULL,
	"info"		VARCHAR(120)	NOT NULL
);
