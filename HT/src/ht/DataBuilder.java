package ht;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * Jun 15, 2018
 * Projekti: ht
 * Tiedosto: DataBuilder
 */
public class DataBuilder {
    static public DataBuilder d = null;
    Connection con = null;
    private Document doc;
    ArrayList<String> offices = new ArrayList();
    ArrayList<String> startAutos = new ArrayList();
    ArrayList<String> endAutos = new ArrayList();
    ArrayList<Object> objects = new ArrayList();
    ArrayList<Package> packages = new ArrayList();
    ArrayList<String> errors = new ArrayList();
    ObservableList<LogData> logs = FXCollections.observableArrayList();
    
    public DataBuilder(){
        this.getOffices();
    }
    // Lisää kaupungit kombobokseihin
    public void updateCombo(){
        // Lisää kaupungit comboboxiin
        try {
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            
            ResultSet rs    = stmt.executeQuery("SELECT \"city\" FROM \"smartpost\""+
                    "GROUP BY \"city\""+
                    "ORDER BY \"city\"");
            
            while (rs.next()) {
                String n = rs.getString("city");
                offices.add(n);
            }
            con.close();
        
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Hakee valmiit esineet tietokannasta ja luo niistä oliot
    public void getObjects(){
        // Etsii tietokannasta valmiit esineet ja luo niistä oliot
        try {
            objects.clear();
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            
            ResultSet rs    = stmt.executeQuery("SELECT * FROM \"esine\""+
                    "GROUP BY \"esineid\""+
                    "ORDER BY \"esineid\"");
            
            while (rs.next()) {
                String n = rs.getString("nimi");
                float k = rs.getFloat("koko");
                float p = rs.getFloat("paino");
                String s = rs.getString("sarkyva");
                int i = rs.getInt("esineid");
                Object o = new Object(n,k,p,s,i);
                objects.add(o);
            }
            con.close();
        
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Hakee valmiit paketit tietokannasta ja luo niistä oliot
    public void getPackages(){
        // Etsii tietokannasta valmiit paketit ja luo niistä oliot
        try {
            packages.clear();
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            
            ResultSet rs    = stmt.executeQuery("SELECT \"paketti\".\"pakettiid\" AS \"pakettiid\","
                    +"\"paketti\".\"luokka\" AS \"luokka\",\"paketti\".\"koko\" AS \"pkoko\","+
                    "\"paketti\".\"painoraja\" AS \"ppaino\",\"esine\".\"nimi\" AS \"nimi\","
                    +" \"esine\".\"esineid\" AS \"esineid\",\"esine\".\"koko\" AS \"ekoko\","+
                    "\"esine\".\"paino\" AS \"epaino\",\"esine\".\"sarkyva\" AS \"sarkyva\" "+ 
                    "FROM \"paketti\" "+
                    "LEFT OUTER JOIN \"esine\" ON \"paketti\".\"esineid\" = \"esine\".\"esineid\"");
            
            while (rs.next()) {
                int id = rs.getInt("luokka");
                float k = rs.getFloat("pkoko");
                float p = rs.getFloat("ppaino");
                String n = rs.getString("nimi");
                float s = rs.getFloat("ekoko");
                float w = rs.getFloat("epaino");
                String f = rs.getString("sarkyva");
                int ei = rs.getInt("esineid");
                int pi = rs.getInt("pakettiid");
                
                Object o = new Object(n,s,w,f,ei);
                //System.out.println(ei+" databasesta haettaessa");
                if (id==1){
                    Package pa = new FirstClass(k,p,o);
                    pa.pakettiid = pi;
                    packages.add(pa);
                }
                if (id==2){
                    Package pa = new SecondClass(k,p,o);
                    pa.pakettiid = pi;
                    packages.add(pa);
                }
                if (id==3){
                    Package pa = new ThirdClass(k,p,o);
                    pa.pakettiid = pi;
                    packages.add(pa);
                }
            }
            con.close();
        
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Singleton
    static public DataBuilder getInstance(){
        // Singletonia varten
        if (d == null){
        d = new DataBuilder();
        }

        return d;
    }
    
    // Päivittää automaatit comboboxiin kaupungin perusteella
    public void updateStartAuto(String s){
        // Päivittää automaatit comboboxiin kaupungin perusteella
        startAutos.clear();
        try {
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            String sql = "SELECT \"nimi\" FROM \"smartpost\""+
                    "WHERE \"city\" = '"+s+"'";
            ResultSet rs    = stmt.executeQuery(sql);
            
            while (rs.next()) {
                String n = rs.getString("nimi");
                startAutos.add(n);
            }
        
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Päivittää automaatit comboboxiin kaupungin perusteella
    public void updateEndAuto(String s){
        // Päivittää automaatit comboboxiin kaupungin perusteella
        endAutos.clear();
        try {
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            String sql = "SELECT \"nimi\" FROM \"smartpost\""+
                    "WHERE \"city\" = '"+s+"'";
            ResultSet rs    = stmt.executeQuery(sql);
            
            while (rs.next()) {
                String n = rs.getString("nimi");
                endAutos.add(n);
            }
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Lukee smartpostit xml:stä
    public void getOffices() {
        // Lukee automaatit xml:stä tietokantaan
        
        try {
            URL url = new URL("http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            
            String content = "";
            String line = "" ;
            
            while ((line = br.readLine()) != null){
                content += line + "\n";    
            } 
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            NodeList nodes = doc.getElementsByTagName("item");
            
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            stmt.executeUpdate("DELETE FROM \"smartpost\"");
            
            for (int i=0;i<nodes.getLength();i++){
                Node node = nodes.item(i);
                Element e = (Element) node;
                int id = i+1;
                String name =((Element)e.getElementsByTagName("name").item(0)).getTextContent();
                String code = ((Element)e.getElementsByTagName("postalcode").item(0)).getTextContent();
                String city =((Element)e.getElementsByTagName("city").item(0)).getTextContent();
                String address =((Element)e.getElementsByTagName("address").item(0)).getTextContent();
                float lat = Float.parseFloat(((Element)e.getElementsByTagName("lat").item(0)).getTextContent());
                float lng = Float.parseFloat(((Element)e.getElementsByTagName("lng").item(0)).getTextContent());
                String avail =((Element)e.getElementsByTagName("availability").item(0)).getTextContent();
                
                stmt.executeUpdate("INSERT INTO \"smartpost\" VALUES("+id+",'"+
                        name+"','"+code+"','"+city+"','"+address+"',"+lat+","+lng+",'"+avail+"')");    
            }
            con.close();
            
        
        } catch (MalformedURLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ParserConfigurationException | SAXException | SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Tarkistaa esineen mahtumisen pakettiin
    public void checkBounds(float is,float ps,float iw,float pw){
        // Tarkistaa esineen pakettiin mahtumisen
        if (is>ps & iw>pw){
            errors.add("Esine ei mahdu pakettiin vaikka miten yrittää tunkea!");
            errors.add("Esine vaikuttaa myös liian painavalta...");
        }
        else if (is>ps & iw<=pw){
            errors.add("Esine ei mahdu pakettiin vaikka miten yrittää tunkea!");
        }
        else if (is<=ps & iw>pw){
            errors.add("Hupsista, esine tuli paketin pohjasta läpi, liikaa painoa");
        }
    }
    
    // Lisää paketin tietokantaan
    public void addPackage(Package p){
        // Lisää paketin tietokantaan
        try {
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            //System.out.println(p.esineid+" databaseen työntäessä");
            stmt.executeUpdate("INSERT INTO \"paketti\" (\"esineid\",\"luokka\",\"koko\",\"painoraja\")"+
                    "VALUES("+p.esineid+","+p.pclass+","+p.sLimit+","+p.wLimit+")");
        
            con.close();
            
            con = DriverManager.getConnection(url2);
            stmt = con.createStatement();
            
            ResultSet rs    = stmt.executeQuery("SELECT * FROM \"paketti\""+
                    " WHERE \"esineid\"="+p.esineid+" AND \"luokka\" ="+p.pclass+
                    " AND \"koko\"="+p.sLimit+" AND \"painoraja\"="+p.wLimit+"");
            
            while (rs.next()) {
                p.pakettiid = rs.getInt("pakettiid");
            } 
            
            packages.add(p);
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PackageWindowController.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    // Lisää lähetyksen tietokantaan
    public void addDelivery(String s,String e,Package p){
        // Lisää lähetyksen tietokantaan
        int startid = 0;
        int endid = 0;
        try {
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();

            ResultSet rs    = stmt.executeQuery("SELECT \"id\" FROM \"smartpost\""+
                    " WHERE \"nimi\"='"+s+"'");
            
            while (rs.next()) {
                startid = rs.getInt("id");
            }
            
            rs    = stmt.executeQuery("SELECT \"id\" FROM \"smartpost\""+
                    " WHERE \"nimi\"='"+e+"'");
            
            while (rs.next()) {
                endid = rs.getInt("id");
            }
        
            con.close();
            
            con = DriverManager.getConnection(url2);
            stmt = con.createStatement();
            
            stmt.executeUpdate("INSERT INTO \"lahetys\" (\"pakettiid\",\"startid\",\"endid\")"+
                    "VALUES("+p.pakettiid+","+startid+","+endid+")");
            
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PackageWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
    // Repii edelliset lokitiedot tietokannasta
    public void getLogData(){
        // Ettii vanhat lokitiedot tietokannasta
        try {
            logs.clear();
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            
            ResultSet rs    = stmt.executeQuery("SELECT * FROM \"logitiedot\"");
            
            while (rs.next()) {
                String t = rs.getString("aika");
                String i = rs.getString("info");
                LogData ld = new LogData(t,i);
                logs.add(ld);
            }
            con.close();
        
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Tallentaa uuden lokitiedon tietokantaan
    public void addLogData(LogData ld){
        // Tallentaa lokitiedon tietokantaan
        try {
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            //System.out.println(p.esineid+" databaseen työntäessä");
            stmt.executeUpdate("INSERT INTO \"logitiedot\" (\"aika\",\"info\") "+
                    "VALUES('"+ld.time+"','"+ld.info+"')");
        
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PackageWindowController.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    // Palauttaa paketin sisällä olevan esineen
    public String getObjectInsidePackage(Package p){
        // Palauttaa paketin sisällä olevan esineen nimen
        String name = "";
        try {
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            
            ResultSet rs    = stmt.executeQuery("SELECT \"paketti\".\"pakettiid\" AS \"id\",\"esine\".\"nimi\" AS \"name\" "+ 
                    "FROM \"paketti\" "+
                    "LEFT OUTER JOIN \"esine\" ON \"paketti\".\"esineid\" = \"esine\".\"esineid\" "+
                    "WHERE \"id\"="+p.pakettiid);
            
            while (rs.next()) {
                name = rs.getString("name");
            }
        }catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return name;
    }
}

