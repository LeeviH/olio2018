/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author n2863
 */
public class HT extends Application{
    public static FXMLLoader Loader = new FXMLLoader();
    public static FXMLDocumentController FXMLController = null;
            
    @Override
    public void start(Stage stage) throws Exception {
        
        //Parent root = Loader.load(getClass().getResource("FXMLDocument.fxml"));
        Pane p = (Pane) Loader.load(getClass().getResource("FXMLDocument.fxml").openStream());
        //Loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
        //Loader.load();
        FXMLController = (FXMLDocumentController) Loader.getController();
        
        Scene scene = new Scene(p);

        stage.setScene(scene);
        stage.show();
        
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public static FXMLDocumentController getFXML(){
        return FXMLController;
    }
}
