/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author n2863
 */
public class PackageWindowController implements Initializable {
    Connection con = null;
    public DataBuilder d = null;
    
    @FXML
    private Button createButton;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField weightField;
    @FXML
    private CheckBox fragileCheck;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        d = DataBuilder.getInstance();
    }    
    
    // Tarkastaa kenttien arvojen oikeellisuudes
    public void checkFields(){
        if (nameField.getText().isEmpty()){
            d.errors.add("Anna esineelle nimi!");
        }
        
        
        try{if (Float.parseFloat(sizeField.getText())<=0){
            d.errors.add("Esineen koko on oltava suurempaa kuin nolla!");
        }}
        catch (NumberFormatException ex){d.errors.add("Esineellä on virheellinen koko!");}
        
        try{if (Float.parseFloat(weightField.getText())<=0){
            d.errors.add("Esineen paino on oltava suurempaa kuin nolla!");
        }}
        catch (NumberFormatException ex){d.errors.add("Esineellä on virheellinen paino!");}
    }
    
    @FXML
    //Luodaan ESINE ja suljetaan ikkuna
    private void createPackage(ActionEvent event) {
        d.errors.clear();
        
        this.checkFields();
        
        if (!d.errors.isEmpty()){
            HT.getFXML().openErrorWindow();
        }
        else{
            String f = "";
            String n = nameField.getText();
            float s = Float.parseFloat(sizeField.getText());
            float w = Float.parseFloat(weightField.getText());
            if (fragileCheck.isSelected()){
                f = "True";
            }
            else{
                f = "False";
            }
            int i = d.objects.size()+1;

            Object o = new Object(n,s,w,f,i);
            d.objects.add(o);

            // Lisätään uusi esine tietokantaan
            try {
                Class.forName("org.sqlite.JDBC");
                String url2 = "jdbc:sqlite:Z:htdb.db";
                con = DriverManager.getConnection(url2);
                Statement stmt = con.createStatement();

                stmt.executeUpdate("INSERT INTO \"esine\" (\"nimi\",\"koko\",\"paino\",\"sarkyva\")"+
                        "VALUES('"+n+"',"+s+","+w+",'"+f+"')");
                con.close();
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(PackageWindowController.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                d.getObjects();
                HT.getFXML().updateObjects();
            }
            catch(Exception e ) {
                System.out.println("Virhe");
            }

            Stage stage = (Stage) createButton.getScene().getWindow();
            stage.close();

            String info = "Luotiin uusi esine nimeltä "+n;
            LogData ld = new LogData(info);
            d.logs.add(ld);
            d.addLogData(ld);
            d.getLogData();
            HT.getFXML().updateLogData();
        }
    }
    
}
