/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {
    public DataBuilder d = null;
    Connection con = null;
    ArrayList<Float> coords = new ArrayList();
    ArrayList<Float> oper = new ArrayList();
    private MediaPlayer mp = null;
    public String info = "";
    
    private Label label;
    @FXML
    public WebView web;
    @FXML
    private Button addToMapButton;
    @FXML
    private ComboBox<String> officeList;
    @FXML
    private Button sendButton;
    
    @FXML
    private RadioButton c1;
    @FXML
    private ToggleGroup classGroup;
    @FXML
    private RadioButton c2;
    @FXML
    private RadioButton c3;
    @FXML
    public ComboBox<Object> ObjectList;
    @FXML
    private Button createObjectButton;
    @FXML
    private ComboBox<String> startCityBox;
    @FXML
    private ComboBox<String> endCityBox;
    @FXML
    private ComboBox<String> startAutoBox;
    @FXML
    private ComboBox<String> endAutoBox;
    @FXML
    private Button deletePathsButton;
    @FXML
    private ComboBox<Float> sizeBox;
    @FXML
    private ListView<Package> storageView;
    @FXML
    private Button sendFromStorageButton;
    @FXML
    private Button storeButton;
    @FXML
    private ComboBox<String> materialBox;
    @FXML
    private Button delPackageButton;
    @FXML
    private Button barbarossaButton;
    @FXML
    private Button infoButton;
    @FXML
    private ListView<String> logTable;
    @FXML
    private Button deleteLogButton;
    @FXML
    private Button delAllPackagesButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        d = DataBuilder.getInstance();
        
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        // Haetaan smartpost paikat tietokannasta ja lisätään ne comboboxeihin
        d.updateCombo();
        for (int i=0;i<d.offices.size();i++){
            officeList.getItems().add(d.offices.get(i));
            startCityBox.getItems().add(d.offices.get(i));
            endCityBox.getItems().add(d.offices.get(i));
        }
        
        // Laitetaan pakettien kokoja listaan
        float k = 100;
        while (k<3000){
            sizeBox.getItems().add(k);
            k += 100;
        }
        while (k<=20000){
            sizeBox.getItems().add(k);
            k += 1000;
        }
        
        // Haetaan valmiit esineet tietokannasta ja lisätään ne comboboxiin
        d.getObjects();
        this.updateObjects();
        
        // Haetaan valmiit paketit tietokannasta ja lisätään ne comboboxiin
        d.getPackages();
        this.updatePackages();
        
        // Haetaan lokitiedot tietokannasta
        d.getLogData();
        this.updateLogData();
        
        // Lisätään materiaalit omaan boksiinsa
        materialBox.getItems().addAll("Paperi, max 3 kg","Pahvi, max 15 kg","Muovi, kestää kaiken");
    }

    // Päivittää esineiden comboboxin
    public void updateObjects(){
        // Päivittää esineiden comboboxin
        ObjectList.getItems().clear();
        
        for (int i=0;i<d.objects.size();i++){
            ObjectList.getItems().add(d.objects.get(i));
        }       
    }
    
    // Päivittää pakettien comboboxin
    public void updatePackages(){
        // Päivittää pakettien comboboxin
        storageView.getItems().clear();
        
        for (int i=0;i<d.packages.size();i++){
            storageView.getItems().add(d.packages.get(i));
        }
    }
    
    // Päivittää lokitiedot välilehteensä
    public void updateLogData(){
        // Päivittää lokitiedot välilehteensä
        logTable.getItems().clear();
        
        for (int i=0;i<d.logs.size();i++){
            logTable.getItems().add(d.logs.get(i).toString());
        }
    }
    
    // Tarkistaa pakettiin liittyvät kentät
    public void checkEmptyFields(){
        // Tarkistaa pakettiin liittyvät kentät
        try{if (ObjectList.getValue().toString().isEmpty()){}}
        catch (NullPointerException ex){d.errors.add("Pakettiin ei ole mitään pantavaa!");}
        
        try{if (sizeBox.getValue().toString().isEmpty()){}}
        catch (NullPointerException ex){d.errors.add("Paketille ei ole määritetty kokoa!");}
        
        try{if (materialBox.getValue().isEmpty()){}}
        catch (NullPointerException ex){d.errors.add("Paketille ei ole määritetty materiaalia!");}  
    }
    
    // Tarkistaa automaattikentät
    public void checkAutomatFields(){
        // Tarkistaa automaattikentät
        try{if (startCityBox.getValue().isEmpty() | startAutoBox.getValue().isEmpty()){}}
        catch (NullPointerException ex){d.errors.add("Paketilla ei ole lähtöpaikkaa!");}
            
        try{if (endCityBox.getValue().isEmpty() | endAutoBox.getValue().isEmpty()){}}
        catch (NullPointerException ex){d.errors.add("Paketilla ei ole määränpäätä!");}
        
        try{if (startAutoBox.getValue().toString().equalsIgnoreCase(endAutoBox.getValue().toString())){
            d.errors.add("Paketin lähtöpaikka ja määränpää ovat samat");}}
        catch (NullPointerException ex){}
    }
    
    // Tarkistaa reitin pituuden
    public void checkRouteLength(double l,int i){
        if (i==1&l>150){
            d.errors.add("1.luokan pakettia ei voi lähettää yli 150 km!");
            d.errors.add("      Tämän hetkinen matka "+String.valueOf(l)+" km");
        }
    }
    
    // Avaa error-ikkunan
    public void openErrorWindow(){
        try {
            Stage errorWindow = new Stage();

            Parent page = FXMLLoader.load(getClass().getResource("ErrorWindow.fxml"));

            Scene scene = new Scene(page);

            errorWindow.setScene(scene);
            errorWindow.initModality(Modality.APPLICATION_MODAL);
            errorWindow.showAndWait();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    @FXML
    // Lisää kartalle kaupungin automaatit
    private void addToMap(ActionEvent event) {
        try {
            String place = "'"+officeList.getValue()+"'";
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            
            // Etsitään tietokannasta valitun paikan perusteella
            String sql = "SELECT \"osoite\",\"posticode\",\"city\",\"nimi\",\"auki\""+
                    "FROM \"smartpost\""+
                    "WHERE \"city\" = "+place;
            Statement pstmt = con.createStatement();
            ResultSet rs = pstmt.executeQuery(sql);
            
            while (rs.next()) {
                // Luetaan ja tallennetaan tiedot tietokannasta parametreihin javascript-kutsua varten
                String p1 = rs.getString("osoite")+","+rs.getString("posticode")+rs.getString("city");
                String p2 = rs.getString("nimi")+", auki: "+rs.getString("auki");
                String pars = "document.goToLocation(\""+p1+"\",\""+p2+"\",\"red\")";
                //Piirretään merkki kartalle
                web.getEngine().executeScript(pars);
                
                info = "Lisättiin kartalle paikkakunnan "+rs.getString("city")+" automaatit";
                
                
            }
            // Lisätään lokitieto
            LogData ld = new LogData(info);
            d.logs.add(ld);
            d.addLogData(ld);
            d.getLogData();
            this.updateLogData();
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    // Lähettää paketin
    private void sendAction(ActionEvent event) {
        float pSize = 0;
        float pWeight = 0;
        
        // Tarkistellaan errorien varalta
        d.errors.clear();
        this.checkEmptyFields();
        this.checkAutomatFields();
        try{
            float iSize = ObjectList.getValue().size;
            pSize = sizeBox.getValue();
            float iWeight = ObjectList.getValue().weight;
            pWeight = 0;
            
            if (materialBox.getValue().contains("Paperi")){
                pWeight = 3;
            }
            else if (materialBox.getValue().contains("Pahvi")){
                pWeight = 15;
            }
            else{
                pWeight = 999999;
            } 

            d.checkBounds(iSize, pSize, iWeight, pWeight);
        } catch (NullPointerException ex){}
        

        
        if (!d.errors.isEmpty()){
            this.openErrorWindow();
        }
        
        else{
            coords.clear();
            try {
                String startPlace = startAutoBox.getValue();
                String endPlace = endAutoBox.getValue();
                String startCity = startCityBox.getValue();
                String endCity = endCityBox.getValue();

                Class.forName("org.sqlite.JDBC");
                String url2 = "jdbc:sqlite:htdb.db";
                con = DriverManager.getConnection(url2);

                // Otetaan databasesta automaattien koordinaatit
                String sql = "SELECT \"lat\",\"long\""+
                        "FROM \"smartpost\""+
                        "WHERE \"nimi\" = '"+startPlace+"'";
                String sql2 = "SELECT \"lat\",\"long\""+
                        "FROM \"smartpost\""+
                        "WHERE \"nimi\" = '"+endPlace+"'";
                Statement pstmt = con.createStatement();
                ResultSet rs = pstmt.executeQuery(sql);

                while (rs.next()) {
                    coords.add(rs.getFloat("lat"));
                    coords.add(rs.getFloat("long"));
                }

                rs = pstmt.executeQuery(sql2);

                while (rs.next()) {
                    coords.add(rs.getFloat("lat"));
                    coords.add(rs.getFloat("long"));
                }

                RadioButton selectedClass = (RadioButton) classGroup.getSelectedToggle();
                int pClass = Integer.parseInt(selectedClass.getText());
                
                String pars = "document.createPath("+coords+", \"red\","+pClass+")";
                String pars2 = "document.getDistance("+coords+")";
                
                // Tarkastetaan pituus
                double l = (Double)web.getEngine().executeScript(pars2);
                d.errors.clear();
                this.checkRouteLength(l,pClass);
                
                // Luodaan paketti
                Object o = ObjectList.getValue();
                Package p = null;

                if (Integer.parseInt(selectedClass.getText())==1){
                    p = new FirstClass(pSize,pWeight,o);
                    d.addPackage(p);
                }
                if (Integer.parseInt(selectedClass.getText())==2){
                    p = new SecondClass(pSize,pWeight,o);
                    d.addPackage(p);
                }
                if (Integer.parseInt(selectedClass.getText())==3){
                    p = new ThirdClass(pSize,pWeight,o);
                    d.addPackage(p);
                }
                this.updatePackages();
                
                if (!d.errors.isEmpty()){
                    this.openErrorWindow();
                }

                else{
                    // Lähetetään paketti, piirretään reitti
                    
                    web.getEngine().executeScript(pars);
;
                    AnimationTimer timer = new AnimationTimer() {
                        double start = 0;
                        double ehto = 0;
                        double goal = Math.pow(10,9);
                        double time = 1;
                        
                        @Override
                        public void handle(long now) {
                            if(start==0){
                                start = now/Math.pow(10,9);
                            }
                            
                            time = now/Math.pow(10,9)-start;
                            
                            if (time>1.5 && ehto == 0){
                                int t = (int)web.getEngine().executeScript("document.getTime()");
                                goal = (double)t/1000;
                                ehto = 1;
                            }
                            
                            if (time>goal){
                                try {
                                    Stage brokenWindow = new Stage();

                                    Parent page = FXMLLoader.load(getClass().getResource("BrokenWindow.fxml"));

                                    Scene scene = new Scene(page);

                                    brokenWindow.setScene(scene);
                                    //brokenWindow.initModality(Modality.APPLICATION_MODAL);
                                    brokenWindow.show();

                                } catch (IOException ex) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                this.stop();
                            }
                        }
                    };
                    if(p.pclass == 1 & p.ob.frag.equalsIgnoreCase("True")){
                        timer.start();
                    }
                    else if ((p.sLimit-p.ob.size)>3000 & p.ob.frag.equalsIgnoreCase("True")){
                        timer.start();
                    }
                    
                    d.addDelivery(startPlace, endPlace, p);

                    d.addDelivery(startPlace, endPlace, p);
                    
                    info = "Lähetettiin "+selectedClass.getText()+".luokan paketti, jonka sisällä oli "+o.name+
                            ", paikasta "+startCity+" paikkaan "+endCity;
                    LogData ld = new LogData(info);
                    d.logs.add(ld);
                    d.addLogData(ld);
                    d.getLogData();
                    this.updateLogData();
                }

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    // Avaa esineen-luonti-ikkunan
    private void openObjectWindow(ActionEvent event) {
        try {
            // Avataan esineen luonti-ikkuna
            Stage packageWindow = new Stage();
            
            Parent page = FXMLLoader.load(getClass().getResource("packageWindow.fxml"));
            
            Scene scene = new Scene(page);
            
            packageWindow.setScene(scene);
            packageWindow.initModality(Modality.APPLICATION_MODAL);
            packageWindow.showAndWait();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    // Näyttää halutun kaupungin automaatit
    private void startBoxClick(ActionEvent event) {
        // Näyttää halutun kaupungin automaatit
        String s = startCityBox.getValue();
        startAutoBox.getItems().clear();
        d.updateStartAuto(s);
        for (int i=0;i<d.startAutos.size();i++){
            startAutoBox.getItems().add(d.startAutos.get(i));
        }
    }

    @FXML
    // Näyttää halutun kaupungin automaatit
    //END box click, typo
    private void sendBoxClick(ActionEvent event) {
        // Näyttää halutun kaupungin automaatit
        String s = endCityBox.getValue();
        endAutoBox.getItems().clear();
        d.updateEndAuto(s);
        for (int i=0;i<d.endAutos.size();i++){
            endAutoBox.getItems().add(d.endAutos.get(i));
        }
    }

    @FXML
    // Poistaa kaikki reitit kartalta
    private void deletePaths(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
        
        info = "Poistettiin kaikki reitit kartalta";
        LogData ld = new LogData(info);
        d.logs.add(ld);
        d.addLogData(ld);
        d.getLogData();
        this.updateLogData();
    }

    @FXML
    // Lähettää varastosta valitun paketin
    private void sendFromStorage(ActionEvent event) {
        d.errors.clear();
        int pClass = 1;
        Package p = null;
        try{
            p = storageView.getSelectionModel().getSelectedItem();
        } catch (NullPointerException ex){d.errors.add("Et ole valinnut pakettia!");}
        this.checkAutomatFields();

        
        if (!d.errors.isEmpty()){
            this.openErrorWindow();
        }
        
        else{
            coords.clear();
            try {
                String startPlace = startAutoBox.getValue();
                String endPlace = endAutoBox.getValue();
                String startCity = startCityBox.getValue();
                String endCity = endCityBox.getValue();

                Class.forName("org.sqlite.JDBC");
                String url2 = "jdbc:sqlite:htdb.db";
                con = DriverManager.getConnection(url2);

                // Otetaan databasesta automaattien koordinaatit
                String sql = "SELECT \"lat\",\"long\""+
                        "FROM \"smartpost\""+
                        "WHERE \"nimi\" = '"+startPlace+"'";
                String sql2 = "SELECT \"lat\",\"long\""+
                        "FROM \"smartpost\""+
                        "WHERE \"nimi\" = '"+endPlace+"'";
                Statement pstmt = con.createStatement();
                ResultSet rs = pstmt.executeQuery(sql);

                while (rs.next()) {
                    coords.add(rs.getFloat("lat"));
                    coords.add(rs.getFloat("long"));
                }

                rs = pstmt.executeQuery(sql2);

                while (rs.next()) {
                    coords.add(rs.getFloat("lat"));
                    coords.add(rs.getFloat("long"));
                }

                // Tarkistetaan reitin pituus
                RadioButton selectedClass = (RadioButton) classGroup.getSelectedToggle();
                String pars = "document.createPath("+coords+", \"red\","+p.pclass+")";
                String pars2 = "document.getDistance("+coords+")";
                
                double l = (Double)web.getEngine().executeScript(pars2);
                d.errors.clear();
                this.checkRouteLength(l,p.pclass);
                
                if (!d.errors.isEmpty()){
                    this.openErrorWindow();
                }

                else{
                    //Piirretään reitti kartalle
                    web.getEngine().executeScript(pars);
                    
                    AnimationTimer timer = new AnimationTimer() {
                        double start = 0;
                        double ehto = 0;
                        double goal = Math.pow(10,9);
                        double time = 1;
                        
                        @Override
                        public void handle(long now) {
                            if(start==0){
                                start = now/Math.pow(10,9);
                            }
                            
                            time = now/Math.pow(10,9)-start;
                            
                            if (time>1.5 && ehto == 0){
                                int t = (int)web.getEngine().executeScript("document.getTime()");
                                goal = (double)t/1000;
                                ehto = 1;
                            }
                            
                            if (time>goal){
                                try {
                                    Stage brokenWindow = new Stage();

                                    Parent page = FXMLLoader.load(getClass().getResource("BrokenWindow.fxml"));

                                    Scene scene = new Scene(page);

                                    brokenWindow.setScene(scene);
                                    //brokenWindow.initModality(Modality.APPLICATION_MODAL);
                                    brokenWindow.show();

                                } catch (IOException ex) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                this.stop();
                            }
                        }
                    };
                    if(p.pclass == 1 & p.ob.frag.equalsIgnoreCase("True")){
                        timer.start();
                    }
                    else if ((p.sLimit-p.ob.size)>3000 & p.ob.frag.equalsIgnoreCase("True")){
                        timer.start();
                    }
                    
                    d.addDelivery(startPlace, endPlace, p);
                    
                    String nn = d.getObjectInsidePackage(p);
                    info = "Lähetettiin "+p.pclass+".luokan paketti, jonka sisällä oli "+nn+
                            ", paikasta "+startCity+" paikkaan "+endCity;
                    LogData ld = new LogData(info);
                    d.logs.add(ld);
                    d.addLogData(ld);
                    d.getLogData();
                    this.updateLogData();
                    
                }
                
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    // Tallentaa paketin varastoon
    private void saveToStorage(ActionEvent event) {
        // Tarkistetaan erroreilta
        d.errors.clear();
        
        try{if (ObjectList.getValue().toString().isEmpty()){}}
        catch (NullPointerException ex){d.errors.add("Pakettiin ei ole mitään pantavaa!");}

        try{if (sizeBox.getValue().toString().isEmpty()){}}
        catch (NullPointerException ex){d.errors.add("Paketille ei ole määritetty kokoa!");}

        try{if (materialBox.getValue().isEmpty()){}}
        catch (NullPointerException ex){d.errors.add("Paketille ei ole määritetty materiaalia!");}
        
        try{
            float iSize = ObjectList.getValue().size;
            float pSize = sizeBox.getValue();
            float iWeight = ObjectList.getValue().weight;
            float pWeight = 0;
            
            if (materialBox.getValue().contains("Paperi")){
                pWeight = 3;
            }
            else if (materialBox.getValue().contains("Pahvi")){
                pWeight = 15;
            }
            else{
                pWeight = 999999;
            } 

            d.checkBounds(iSize, pSize, iWeight, pWeight);
        } catch (NullPointerException ex){}
        
        
        if (!d.errors.isEmpty()){
            this.openErrorWindow();
        }
        
        else{
            float pSize = sizeBox.getValue();
            float pWeight = 0;
            Object o = ObjectList.getValue();
            
            if (materialBox.getValue().contains("Paperi")){
                pWeight = 3;
            }
            else if (materialBox.getValue().contains("Pahvi")){
                pWeight = 15;
            }
            else{
                pWeight = 999999;
            }
            
            // Luodaan paketti
            RadioButton selectedClass = (RadioButton) classGroup.getSelectedToggle();
            if (Integer.parseInt(selectedClass.getText())==1){
                Package p = new FirstClass(pSize,pWeight,o);
                d.addPackage(p);
            }
            if (Integer.parseInt(selectedClass.getText())==2){
                Package p = new SecondClass(pSize,pWeight,o);
                d.addPackage(p);
            }
            if (Integer.parseInt(selectedClass.getText())==3){
                Package p = new ThirdClass(pSize,pWeight,o);
                d.addPackage(p);
            }
            this.updatePackages();
            
            info = "Tallennettiin "+selectedClass.getText()+".luokan paketti, jonka sisällä oli "+o.name;
            LogData ld = new LogData(info);
            d.logs.add(ld);
            d.addLogData(ld);
            d.getLogData();
            this.updateLogData();
        }
    }

    @FXML
    // Poistaa valitun paketin varastosta
    private void deletePackage(ActionEvent event) {
        d.errors.clear();
        Package p = null;
        try{
            p = storageView.getSelectionModel().getSelectedItem();
        } catch (NullPointerException ex){d.errors.add("Et ole valinnut pakettia!");}
        if (!d.errors.isEmpty()){
            this.openErrorWindow();
        }
        
        else{
            try {
                String nn = d.getObjectInsidePackage(p);
                info = "Poistettiin "+p.pclass+".luokan paketti, jonka sisällä oli "+nn;
                LogData ld = new LogData(info);
                d.logs.add(ld);
                d.addLogData(ld);
                d.getLogData();
                this.updateLogData();
                
                Class.forName("org.sqlite.JDBC");
                String url2 = "jdbc:sqlite:htdb.db";
                con = DriverManager.getConnection(url2);
                Statement stmt = con.createStatement();
                stmt.executeUpdate("DELETE FROM \"paketti\" "+
                        "WHERE \"pakettiid\" = "+p.pakettiid);
                
                con.close();
                
                d.getPackages();
                this.updatePackages();

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(PackageWindowController.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }

    @FXML
    private void startOperation(ActionEvent event) {
        oper.add(Float.parseFloat("53.891516"));
        oper.add(Float.parseFloat("14.208367"));
        oper.add(Float.parseFloat("58.507215"));
        oper.add(Float.parseFloat("31.230562"));
        String pars = "document.createPath("+oper+", \"red\","+1+")";
        web.getEngine().executeScript(pars);
        oper.clear();
        
        oper.add(Float.parseFloat("52.315242"));
        oper.add(Float.parseFloat("14.574871"));
        oper.add(Float.parseFloat("57.263798"));
        oper.add(Float.parseFloat("32.916597"));
        pars = "document.createPath("+oper+", \"red\","+1+")";
        web.getEngine().executeScript(pars);
        oper.clear();
        
        oper.add(Float.parseFloat("51.186922"));
        oper.add(Float.parseFloat("14.988496"));
        oper.add(Float.parseFloat("54.672614"));
        oper.add(Float.parseFloat("37.504321"));
        pars = "document.createPath("+oper+", \"red\","+1+")";
        web.getEngine().executeScript(pars);
        oper.clear();
        
        oper.add(Float.parseFloat("49.632438"));
        oper.add(Float.parseFloat("12.494511"));
        oper.add(Float.parseFloat("52.220248"));
        oper.add(Float.parseFloat("39.149771"));
        pars = "document.createPath("+oper+", \"red\","+1+")";
        web.getEngine().executeScript(pars);
        oper.clear();
        
        oper.add(Float.parseFloat("48.464003"));
        oper.add(Float.parseFloat("13.429769"));
        oper.add(Float.parseFloat("48.703676"));
        oper.add(Float.parseFloat("44.405964"));
        pars = "document.createPath("+oper+", \"red\","+1+")";
        web.getEngine().executeScript(pars);
        oper.clear();
        
        oper.add(Float.parseFloat("53.165364"));
        oper.add(Float.parseFloat("14.076245"));
        oper.add(Float.parseFloat("58.507215"));
        oper.add(Float.parseFloat("31.230562"));
        oper.add(Float.parseFloat("52.315242"));
        oper.add(Float.parseFloat("14.574871"));
        oper.add(Float.parseFloat("57.263798"));
        oper.add(Float.parseFloat("32.916597")); 
        oper.add(Float.parseFloat("51.186922"));
        oper.add(Float.parseFloat("14.988496"));
        oper.add(Float.parseFloat("54.672614"));
        oper.add(Float.parseFloat("37.504321"));
        oper.add(Float.parseFloat("49.632438"));
        oper.add(Float.parseFloat("12.494511"));
        oper.add(Float.parseFloat("52.220248"));
        oper.add(Float.parseFloat("39.149771"));
        oper.add(Float.parseFloat("48.464003"));
        oper.add(Float.parseFloat("13.429769"));
        oper.add(Float.parseFloat("48.703676"));
        oper.add(Float.parseFloat("44.405964"));
        pars = "document.flag("+oper+")";
        web.getEngine().executeScript(pars);
        oper.clear();
        
        try {
            String path = getClass().getResource("audiosaksa.mp3").toURI().toString();
            Media media = new Media(path);
            mp = new MediaPlayer(media);
            mp.play();
        } catch (URISyntaxException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        info = "Aloitettiin operaatio barbarossa";
        LogData ld = new LogData(info);
        d.logs.add(ld);
        d.addLogData(ld);
        d.getLogData();
        this.updateLogData();
        
    }

    @FXML
    // Avaa info-ikkunan
    private void openInfo(ActionEvent event) {
        // Avaa paketti-infoikkunan
        try {
            Stage packageWindow = new Stage();
            
            Parent page = FXMLLoader.load(getClass().getResource("infoWindow.fxml"));
            
            Scene scene = new Scene(page);
            
            packageWindow.setScene(scene);
            packageWindow.showAndWait();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    // Poistaa lokitiedot
    private void deleteLog(ActionEvent event) {
        // Tyhjentää lokitiedot
        d.logs.clear();
        
        try {
                Class.forName("org.sqlite.JDBC");
                String url2 = "jdbc:sqlite:htdb.db";
                con = DriverManager.getConnection(url2);
                Statement stmt = con.createStatement();
                //System.out.println(p.esineid+" databaseen työntäessä");
                stmt.executeUpdate("DELETE FROM \"logitiedot\" ");

                con.close();

                d.getLogData();
                this.updateLogData();

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(PackageWindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    @FXML
    // Poistaa kaikki paketit varastosta
    private void delAllPackages(ActionEvent event) {
        // Poistaa kaikki paketit varastosta
        d.packages.clear();
        
        try {
                Class.forName("org.sqlite.JDBC");
                String url2 = "jdbc:sqlite:htdb.db";
                con = DriverManager.getConnection(url2);
                Statement stmt = con.createStatement();
                //System.out.println(p.esineid+" databaseen työntäessä");
                stmt.executeUpdate("DELETE FROM \"paketti\" ");

                con.close();

                d.getPackages();
                this.updatePackages();
                
                info = "Poistettiin kaikki paketit";
                LogData ld = new LogData(info);
                d.logs.add(ld);
                d.addLogData(ld);
                d.getLogData();
                this.updateLogData();

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(PackageWindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
}
