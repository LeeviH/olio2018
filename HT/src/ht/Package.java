package ht;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * Jun 15, 2018
 * Projekti: ht
 * Tiedosto: Package
 */
abstract class Package {
    
    public int pclass;
    public int esineid;
    public float sLimit;
    public float wLimit;
    public Object ob =null;
    Connection con = null;
    public DataBuilder d = null;
    public String objName;
    public float objSize;
    public float objWeight;
    public String objFragile;
    public int pakettiid;
    public String q = ""; //apumuuttuja esillepanoon
    
    public Package(float s,float w,Object o){
        d = DataBuilder.getInstance();
        sLimit = s;
        wLimit = w;
        ob = o;
        objFragile = ob.frag;
        objSize = ob.size;
        objWeight = ob.weight;
        pakettiid = 0;
        try {
            Class.forName("org.sqlite.JDBC");
            String url2 = "jdbc:sqlite:htdb.db";
            con = DriverManager.getConnection(url2);
            Statement stmt = con.createStatement();
            
            ResultSet rs    = stmt.executeQuery("SELECT * FROM \"esine\""+
                    " WHERE \"nimi\"='"+ob.name+"' AND \"koko\" ="+ob.size+
                    " AND \"paino\"="+ob.weight+" AND \"sarkyva\"='"+ob.frag+"'");
            
            while (rs.next()) {
                esineid = rs.getInt("esineid");
                //System.out.println(esineid+" pakettia luodessa,package.java");
            }
            con.close();

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(PackageWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public String toString(){
        if (this.objFragile.equalsIgnoreCase("True")){
            q = " (Särkyvä)";
        }
        return sLimit+" cm3:n kokoinen "+pclass+". luokan paketti, jonka sisällä on "+ob.name+" ("+
                ob.size+" cm3, "+ob.weight+" kg"+q+")";
    }

}

class FirstClass extends Package{
    public FirstClass(float s,float w,Object o){
        super(s,w,o);
        pclass = 1;
    }
}

class SecondClass extends Package{
    public SecondClass(float s,float w,Object o){
        super(s,w,o);
        pclass = 2;
    }
}

class ThirdClass extends Package{
    public ThirdClass(float s,float w,Object o){
        super(s,w,o);
        pclass = 3;
    }
}