/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author n2863
 */
public class ErrorWindowController implements Initializable {

    public DataBuilder d = null;
    public String errorText = "";
    
    @FXML
    private Button closeButton;
    @FXML
    private Label errorField;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        d = DataBuilder.getInstance();
        errorField.getText().replace(errorField.getText(),"");
        for (String s:d.errors){
            errorText += s +"\n";
        }
        errorField.setText(errorText);
    }  

    @FXML
    private void close(ActionEvent event) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
    
}
