package ht;

import java.util.ArrayList;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * Jun 18, 2018
 * Projekti: ht
 * Tiedosto: Object
 */
public class Object {
    public String name;
    public float weight;
    public float size;
    public String frag;
    public int id;
    public String q = ""; //apumuuttuja esillepanoon
    
    public Object(String n,float s,float w,String f,int i){
        name = n;
        size = s;
        weight = w;
        frag = f;
        id = i;
    }
    
    @Override
    public String toString(){
        if (this.frag.equalsIgnoreCase("True")){
            q = "(Särkyvä)";
        }
        return name+" "+size+" cm3, "+ weight+" kg "+q;
    }
}
