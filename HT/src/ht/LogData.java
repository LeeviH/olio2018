package ht;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * Jul 5, 2018
 * Projekti: ht
 * Tiedosto: LogData
 */
public class LogData {
    public String time;
    public String info;
    
    Date dNow = new Date();
    SimpleDateFormat ft = new SimpleDateFormat ("dd.MM.yyyy HH:mm:ss");
    
    public LogData(String s){
        time = ft.format(dNow);
        info = s;
    }
    
    public LogData(String t,String s){
        time = t;
        info = s;
    }
    
    @Override
    public String toString(){
        return time+"   "+info;
    }
}
