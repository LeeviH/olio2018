/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;

/**
 *
 * @author n2863
 */
public class BottleDispenser {
    private int bottles;
    private float money;
    private int k;

    public BottleDispenser() {
        bottles = 6;
        money = 0;
        
        Bottle b = new Bottle("Pepsi Max","Pepsi",(float) 0.3,(float) 0.5,(float) 1.80);
        bottleList.add(b);
        Bottle bb = new Bottle("Pepsi Max","Pepsi",(float) 0.3,(float) 1.5,(float) 2.20);
        bottleList.add(bb);
        Bottle bc = new Bottle("Coca-Cola Zero","Coca-Cola",(float) 0.3,(float) 0.5,(float) 2.0);
        bottleList.add(bc);
        Bottle bd = new Bottle("Coca-Cola Zero","Coca-Cola",(float) 0.3,(float) 1.5,(float) 2.5);
        bottleList.add(bd);
        Bottle be = new Bottle("Fanta Zero","Fanta",(float) 0.3,(float) 0.5,(float) 1.95);
        bottleList.add(be);
        Bottle bf = new Bottle("Fanta Zero","Fanta",(float) 0.3,(float) 0.5,(float) 1.95);
        bottleList.add(bf);
    }
    
    
    private ArrayList<Bottle> bottleList = new ArrayList();
    
    
    public void printList(){
        for (k=0; k<bottles; k=k+1){
            System.out.println(k+1+". Nimi: "+bottleList.get(k).name);
            System.out.println("	Koko: "+bottleList.get(k).size	+"	Hinta: "+bottleList.get(k).cost);
        }
    }

    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle(int x) {
        Bottle b = bottleList.get(x-1);
        
        if (money<b.cost){
            System.out.println("Syötä rahaa ensin!");
        }
        else{
            bottles -= 1;
            bottleList.remove(x-1);
            money -= b.cost;
            System.out.println("KACHUNK! "+b.name+" tipahti masiinasta!");
        }
    }

    public void returnMoney() {
        System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€",money);
        System.out.println("");
    }
}