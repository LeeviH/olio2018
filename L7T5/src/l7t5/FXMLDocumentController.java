/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l7t5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private Button load;
    @FXML
    private Button save;
    @FXML
    private TextField filenameSave;
    @FXML
    private TextArea inputText;
    @FXML
    private TextField filenameLoad;
    @FXML
    private TextArea outputText;

    private void handleButtonAction(ActionEvent event) {
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void loadFile(ActionEvent event) throws IOException {
        BufferedReader in = null;
        String line = "";
        String fulltext = "";

        try {
            in = new BufferedReader(new FileReader(filenameLoad.getText()));
            line = in.readLine();

            while (line != null) {
                fulltext += line + "\n";
                line = in.readLine();
            }
            in.close();
            outputText.setText(fulltext);

        } catch (FileNotFoundException ex) {
            outputText.setText("Kysymääsi tiedostoa ei löytynyt!!!");
        }

        
    }

    @FXML
    private void saveFile(ActionEvent event) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(filenameSave.getText()));
        out.write(inputText.getText());
        out.close();
    }

}
