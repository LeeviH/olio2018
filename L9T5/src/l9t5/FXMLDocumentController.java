/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l9t5;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {
    movieInfo m = null;
    
    @FXML
    private Label label;
    @FXML
    private Button listButton;
    @FXML
    private Button searchButton;
    @FXML
    private ComboBox<Theatre> theaterList;
    @FXML
    private TextField startInput;
    @FXML
    private TextField endIput;
    @FXML
    private TextField dateInput;
    @FXML
    private TextField nameInput;
    @FXML
    private ListView<String> movieList;
    @FXML
    private Label searchResultTitle;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        m = movieInfo.getInstance();
        
        for (int i=0;i<=m.theatreCount-1;i++){
            theaterList.getItems().add(m.getTheatre(i));
        }
    }    

    @FXML
    private void listMovies(ActionEvent event) {
        movieList.getItems().clear();
        Date dNow = new Date();
        SimpleDateFormat ft = 
            new SimpleDateFormat ("dd.MM.yyyy");
        
        String day = dateInput.getText();
        
        try{
            if (day == null){
                day = ft.format(dNow).toString();
                m.getMovies(theaterList.getValue().id,day);
                searchResultTitle.setText("Hakutulokset:");
            }
            else{
                m.getMovies(theaterList.getValue().id,day);
                searchResultTitle.setText("Hakutulokset:");
            }
        }catch (NullPointerException ex){}
        
        String end,beg;
        
        for (Movie i : m.movies){
            if (startInput.getText().isEmpty()){
                beg = "0";
            }
            else{
                beg = startInput.getText();
            }
            if (endIput.getText().isEmpty()){
                end = "2400";
            }
            else{
                end = endIput.getText();
            }

            end = end.replace(":", "");
            beg = beg.replace(":", "");
            String tim = i.start.replace(":", "");

            int endi = Integer.parseInt(end);
            int starti = Integer.parseInt(beg);
            int time = Integer.parseInt(tim);
            
            if (time>=starti&time<=endi){
                movieList.getItems().add(i.title+" - "+i.place+" klo "+i.start);
            }
        }
    }

    @FXML
    private void searchName(ActionEvent event) {
        movieList.getItems().clear();
        Date dNow = new Date();
        SimpleDateFormat ft = 
            new SimpleDateFormat ("dd.MM.yyyy");
        
        String day;
        
        if (nameInput.getText().isEmpty()){
            movieList.getItems().add("Syötä hakutermi!");
        }
        else{
            movieList.getItems().clear();
            if (dateInput.getText().isEmpty()){
                day = ft.format(dNow).toString();
                m.searchName(nameInput.getText(),day);
            }
            else{
                day = dateInput.getText();
                m.searchName(nameInput.getText(),day);
            }
            
            String end,beg;
            searchResultTitle.setText("Hakutulokset termillä '"+nameInput.getText()+"':");
            for (Movie i : m.movies){
                if (startInput.getText().isEmpty()){
                    beg = "0";
                }
                else{
                    beg = startInput.getText();
                }
                if (endIput.getText().isEmpty()){
                    end = "2400";
                }
                else{
                    end = endIput.getText();
                }

                end = end.replace(":", "");
                beg = beg.replace(":", "");
                String tim = i.start.replace(":", "");

                int endi = Integer.parseInt(end);
                int starti = Integer.parseInt(beg);
                int time = Integer.parseInt(tim);

                if (time>=starti&time<=endi){
                    movieList.getItems().add(i.title+" - "+i.place+" klo "+i.start);
                }
            }
            if (movieList.getItems().isEmpty()){
                movieList.getItems().add("Haullasi ei löytynyt yhtään tulosta");
            }
        }
    }
    
}
