package l11t3;

import java.util.ArrayList;
import l11t3.FXMLDocumentController.Point;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * Jun 11, 2018
 * Projekti: l11t3
 * Tiedosto: ShapeHandler
 */
public class ShapeHandler {
        static public ShapeHandler s = null;
        public ArrayList<Point> points = new ArrayList();
        
        private ShapeHandler(){
            
        }
        static public ShapeHandler getInstance(){
            if (s == null){
            s = new ShapeHandler();
            }
        
            return s;
        }
    }
