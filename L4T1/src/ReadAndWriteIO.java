
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * May 29, 2018
 * Projekti: 
 * Tiedosto: ReadAndWriteIO
 */
public class ReadAndWriteIO {
    private String filename;
    private String line;
    private String fulltext = "";
    
    public ReadAndWriteIO(String s){
        filename = s;
    }
    
    public void writeText(String text) throws IOException{
        BufferedWriter out = new BufferedWriter(new FileWriter(filename));
        out.write(text);
        out.close();
    }
    
    public String readText() throws IOException{
        BufferedReader in = new BufferedReader(new FileReader(filename));
        line = in.readLine();
        
        while (line != null) {
            fulltext += line +"\n";
            line = in.readLine();
        }
        in.close();
        
        return fulltext;
    }
}
