
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * May 29, 2018
 * Projekti: 
 * Tiedosto: Mainclass
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String filename = "input.txt";
        String text = "";
        String content = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        /*System.out.print("Anna tiedoston nimi: ");
        try {
            filename = br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Mainclass.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.print("Anna kirjoitettava teksti: ");
        try {
            text = br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Mainclass.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        */
        ReadAndWriteIO raw = new ReadAndWriteIO(filename);
            
        /*try {
            raw.writeText(text);
        } catch (IOException ex) {
            Logger.getLogger(Mainclass.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        try {
            content = raw.readText();
        } catch (IOException ex) {
            Logger.getLogger(Mainclass.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.print(content);
    }

}
