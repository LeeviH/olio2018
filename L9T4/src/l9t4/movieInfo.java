package l9t4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * Jun 4, 2018
 * Projekti: l9t2
 * Tiedosto: movieInfo
 */
public class movieInfo {
    static private movieInfo m = null;
    private Document doc;
    
    ArrayList<Theatre> theatreList = new ArrayList();
    ArrayList<Movie> movies = new ArrayList();
    
    public int theatreCount = 0;
    
    public movieInfo(){
        
        this.getTheatres();
        
    }
    
    public void getTheatres() {
        try {
            URL url = new URL("https://www.finnkino.fi/xml/TheatreAreas/");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            
            String content = "";
            String line = "" ;
            
            while ((line = br.readLine()) != null){
                content += line + "\n";    
            }
                    
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            NodeList nodes = doc.getElementsByTagName("TheatreArea");
            
            for (int i=1;i<nodes.getLength();i++){
                Node node = nodes.item(i);
                Element e = (Element) node;
                
                String name = ((Element)e.getElementsByTagName("Name").item(0)).getTextContent();
                String id = ((Element)e.getElementsByTagName("ID").item(0)).getTextContent();
                
                Theatre t = new Theatre(name,id);
                theatreList.add(t);
            }
            theatreCount = theatreList.size();
            
        
        } catch (MalformedURLException ex) {
            Logger.getLogger(movieInfo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(movieInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public void getMovies(String s,String t){
        
        movies.clear();
        
        try {
            URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area="+s+"&dt="+t);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            
            String content = "";
            String line = "" ;
            
            while ((line = br.readLine()) != null){
                content += line + "\n";    
            }
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            NodeList nodes = doc.getElementsByTagName("Show");
            
            for (int i=0;i<nodes.getLength();i++){
                Node node = nodes.item(i);
                Element e = (Element) node;
                
                String title = ((Element)e.getElementsByTagName("Title").item(0)).getTextContent();
                String place = ((Element)e.getElementsByTagName("Theatre").item(0)).getTextContent();
                String start = ((Element)e.getElementsByTagName("dttmShowStart").item(0)).getTextContent();
                start = start.substring(11,16);
                
                Movie m = new Movie(title,place,start);
                movies.add(m);
            }
            
        
        } catch (MalformedURLException ex) {
            Logger.getLogger(movieInfo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(movieInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Theatre getTheatre(int i){
        return theatreList.get(i);
    }
        

    
    static public movieInfo getInstance(){
        if (m == null){
            m = new movieInfo();
        }
        
        return m;
    }
}

class Theatre{
    public String name;
    public String id;

    public Theatre(String s,String d){
        name = s;
        id = d;
    }

    @Override
    public String toString(){
        return name;
    }
}

class Movie{
    public String title;
    public String place;
    public String start;
    
    public Movie(String t,String p,String s){
        title = t;
        place = p;
        start = s;
    }
}