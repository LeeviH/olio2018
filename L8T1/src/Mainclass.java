/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author n2863
 */
import java.util.Scanner;
/**
 *
 * @author n2863
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int choice = 5;
        int bottlenro = 0;
        
        BottleDispenser b1 = BottleDispenser.getInstance();
        
        Scanner scan = new Scanner(System.in);
        
        while (choice != 0){
            System.out.println("");
            System.out.println("*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            
            System.out.print("Valintasi: ");
            choice = scan.nextInt();
            
            if (choice == 1){
                b1.addMoney();
            }
            
            else if (choice == 2){
                b1.printList();
                System.out.print("Valintasi: ");
                bottlenro = scan.nextInt();
                b1.buyBottle(bottlenro);
            }
            
            else if (choice == 3){
                b1.returnMoney();
            }
            
            else if (choice == 4){
                b1.printList();
            }
            
            else{
                break;
            }
        }
        
    }
    
}
