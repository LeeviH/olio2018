/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author n2863
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BottleDispenser b1 = new BottleDispenser();
        
        b1.addMoney();
        b1.buyBottle();
        b1.buyBottle();
        b1.addMoney();
        b1.addMoney();
        b1.buyBottle();
        b1.returnMoney();
        
    }
    
}
