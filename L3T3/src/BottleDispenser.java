/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author n2863
 */
public class BottleDispenser {
    private int bottles;
    private int money;

    public BottleDispenser() {
        bottles = 5;
        money = 0;
    }

    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle() {
        Bottle b = new Bottle("Pepsi Max","Pepsi",(float) 0.3,(float) 0.5,(float) 1.80);
        
        if (money<b.cost){
            System.out.println("Syötä rahaa ensin!");
        }
        else{
            bottles -= 1;
            money -= 1;
            System.out.println("KACHUNK! "+b.name+" tipahti masiinasta!");
        }
    }

    public void returnMoney() {
        money = 0;
        System.out.println("Klink klink. Sinne menivät rahat!");
    }
}
