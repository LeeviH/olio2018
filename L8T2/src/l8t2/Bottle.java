package l8t2;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * Jun 1, 2018
 * Projekti: l8t2
 * Tiedosto: Bottle
 */
public class Bottle {
    public String name;
    public String manufacturer;
    public float energy;
    public float size;
    public float cost;
    
    public Bottle(String neim,String manu,float en,float sz, float $){
        name = neim;
        manufacturer = manu;
        energy = en;
        size = sz;
        cost = $;
    }
    
    @Override
    public String toString(){
        return name +" ("+size+" l): "+cost+" €";
    }
    
    public float getCost(){
        return cost;
    }
}
