import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * May 29, 2018
 * Projekti: 
 * Tiedosto: ReadAndWriteIO
 */
public class ReadAndWriteIO {
    private String filenameI;
    private String newfilename;
    private String line;
    private String fulltext = "";
    
    public ReadAndWriteIO(String i){
        filenameI = i;
    }
    
    public void unzip() throws FileNotFoundException, IOException{
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(filenameI));
        ZipEntry zipEntry = zis.getNextEntry();
        while(zipEntry != null){
            int len;
            while ((len = zis.read(buffer)) > 0) {
                line = new String(buffer, 0, len);
                System.out.println(line);
            }
            
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }
    
}