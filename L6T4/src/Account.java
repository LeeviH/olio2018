
import java.util.Scanner;

/**
 * Olio-ohjelmointi kesäkurssi 2018
 * Leevi Hovatov
 *
 * May 31, 2018
 * Projekti: 
 * Tiedosto: Account
 */
abstract class Tili{
    protected String tilinro;
    protected int money;
    protected int luotto;
    
    Scanner scan = new Scanner(System.in);
    
    public Tili(){
        System.out.print("Syötä tilinumero: ");
        tilinro = scan.next();
        System.out.print("Syötä rahamäärä: ");
        money = scan.nextInt();
    }
    
}

class normiTili extends Tili{}

class luottoTili extends Tili{
    public luottoTili(){
        System.out.print("Syötä luottoraja: ");
        luotto = scan.nextInt();
    }
}
