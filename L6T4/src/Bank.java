
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author n2863
 */
public class Bank {
    ArrayList<Tili> tilit = new ArrayList();
    Scanner scan = new Scanner(System.in);
    private int i;
    private String nro;
    private int moveMoney;
       
    public Bank(){
        
    }
    
    public void addNormiTili(){
        Tili uusTili = new normiTili();
        tilit.add(uusTili);
        System.out.println("Tili luotu.");
    }
    
    public void addLuottoTili(){
        Tili uusTili = new luottoTili();
        tilit.add(uusTili);
        System.out.println("Tili luotu.");
    }
    
    private Tili findTili(String s){
        Tili found = null;
        
        for (Tili t:tilit){
            if (t.tilinro.equals(s)){
                found = t;
            }
        }
        
        if (found == null){
            System.out.println("Tiliä ei löytynyt!");
            return null;
        }
        else{
            return found;
        }
    }
    
    public void addMoney(){
        System.out.print("Syötä tilinumero: ");
        nro = scan.next();
        
        System.out.print("Syötä rahamäärä: ");
        moveMoney = scan.nextInt();
        
        this.findTili(nro).money += moveMoney;

    }
    
    public void getMoney(){
        System.out.print("Syötä tilinumero: ");
        nro = scan.next();
        
        System.out.print("Syötä rahamäärä: ");
        moveMoney = scan.nextInt();
        
        this.findTili(nro).money -= moveMoney;
    }
    
    public void deleteTili(){
        System.out.print("Syötä poistettava tilinumero: ");
        nro = scan.next();
        
        i = tilit.indexOf(this.findTili(nro));
        tilit.remove(i);
        
        System.out.println("Tili poistettu.");
    }
    
    public void printOne(){
        System.out.print("Syötä tulostettava tilinumero: ");
        nro = scan.next();
        
        System.out.println("Tilinumero: "+this.findTili(nro).tilinro+" Tilillä rahaa: "+this.findTili(nro).money);
    }
    
    public void printAll(){
        System.out.println("Kaikki tilit:");
    }
}

