/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l11t2v2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private AnchorPane bg;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void clickAction(MouseEvent event) {
        Point p = new Point(event.getX(),event.getY());
        }
    
    public class Point{
       private String name;
       
       public Point(double x,double y){
            Circle c = new Circle(x,y,40);
            c.setFill(Color.WHITE);
            //bg.getChildren().add(c);
            
            Rectangle r = new Rectangle(x-50,y-50,100,100);
            r.setFill(Color.RED);

            Path path = new Path();
            path.setStrokeWidth(7);
            path.setRotate(45);
            MoveTo moveTo = new MoveTo(x+25, y+25);  
            LineTo line1 = new LineTo(x+25, y);  
            LineTo line2 = new LineTo(x-25,y);       
            LineTo line3 = new LineTo(x-25,y-25);  
            LineTo line4 = new LineTo(x-25,y);   
            LineTo line5 = new LineTo(x, y);
            LineTo line6 = new LineTo(x, y-25);
            LineTo line7 = new LineTo(x+25, y-25);
            LineTo line8 = new LineTo(x, y-25);
            LineTo line9 = new LineTo(x, y+25);
            LineTo line10 = new LineTo(x-25, y+25);
            path.getElements().add(moveTo); 
            path.getElements().addAll(line1, line2, line3, line4, line5,line6,line7,line8,line9,line10);
            Group root = new Group(path);
            
            
            bg.getChildren().addAll(r,c,root);
            //bg.getChildren().add(r);
            //bg.getChildren().add(root);
       } 
       
       public void speak(){
           System.out.println("Hei olen piste!");
       }
    }
}