/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l8t3;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {
    
    BottleDispenser b = null;
    int i= 0;
    DecimalFormat df = new DecimalFormat("#.##");
    
    private Label label;
    @FXML
    private Button addMoneyButton;
    @FXML
    private ComboBox<Bottle> bottleList;
    @FXML
    private Button buyButton;
    @FXML
    private Button moneyOutButton;
    @FXML
    private Label moneayAmount;
    @FXML
    private Label message;
    @FXML
    private Label moneyOut;
    @FXML
    private Slider MoneySlider;
    @FXML
    private Label sliderAmount;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        b = BottleDispenser.getInstance();
        int bottles = b.bottles;
        
        for (i=0;i<=bottles-1;i++){
            bottleList.getItems().add(b.getBottle(i));
        }
        
        moneayAmount.setText(String.format("%.2f",b.money)+" €");
        sliderAmount.setText("0,00 €");
        
    }    

    @FXML
    private void addMoney(ActionEvent event) {
        b.money += MoneySlider.getValue();
        moneayAmount.setText(String.format("%.2f",b.money)+" €");
        MoneySlider.setValue(0);
        sliderAmount.setText("0,00 €");
    }

    @FXML
    private void buyAction(ActionEvent event) {
        try{
            

        if (b.money < bottleList.valueProperty().getValue().getCost()){
            message.setText("Rahasi eivät riitä!");
        }
                
        else{
            message.setText("Kachunk! "+bottleList.getValue().name+" putosi masiinasta");
            b.money -= bottleList.getValue().cost;
            moneayAmount.setText(String.format("%.2f",b.money)+" €");
            bottleList.getItems().remove(bottleList.getValue());
            b.removeBottle(bottleList.getValue());
            b.bottles -= 1;

        }
        }
        catch (NullPointerException ex){
            message.setText("Et ole valinnut pulloa tai ne ovat loppu.");
        }
    }

    @FXML
    private void moneyOut(ActionEvent event) {
        moneyOut.setText("Klink klink. Rahaa tuli ulos "+String.format("%.2f",b.money)+" €");
        b.money = 0;
        moneayAmount.setText(Float.toString(b.money)+" €");
    }

    @FXML
    private void slide(MouseEvent event) {
        sliderAmount.setText(String.format("%.2f",MoneySlider.getValue())+" €");
    }

}