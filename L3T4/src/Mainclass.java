/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;
/**
 *
 * @author n2863
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int valinta = 5;
        
        BottleDispenser b1 = new BottleDispenser();
        
        Scanner scan = new Scanner(System.in);
        
        while (valinta != 0){
            System.out.println("");
            System.out.println("*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            
            System.out.print("Valintasi: ");
            valinta = scan.nextInt();
            
            if (valinta == 1){
                b1.addMoney();
            }
            
            else if (valinta == 2){
                b1.buyBottle();
            }
            
            else if (valinta == 3){
                b1.returnMoney();
            }
            
            else if (valinta == 4){
                b1.printList();
            }
            
            else{
                break;
            }
        }
        
    }
    
}
