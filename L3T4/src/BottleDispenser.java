
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author n2863
 */
public class BottleDispenser {
    private int bottles;
    private int money;
    private int k;

    public BottleDispenser() {
        bottles = 6;
        money = 0;
        
        for (k=1; k<= bottles; k=k+1){
        Bottle b = new Bottle("Pepsi Max","Pepsi",(float) 0.3,(float) 0.5,(float) 1.80);
        bottleList.add(b);
        }
    }
    
    
    private ArrayList<Bottle> bottleList = new ArrayList();
    
    
    public void printList(){
        for (k=0; k<bottles; k=k+1){
            System.out.println(k+1+". Nimi: "+bottleList.get(k).name);
            System.out.println("	Koko: "+bottleList.get(k).size	+"	Hinta: "+bottleList.get(k).cost);
        }
    }

    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle() {
        Bottle b = new Bottle("Pepsi Max","Pepsi",(float) 0.3,(float) 0.5,(float) 1.80);
        
        if (money<b.cost){
            System.out.println("Syötä rahaa ensin!");
        }
        else{
            bottles -= 1;
            bottleList.remove(bottles-1);
            money -= 1;
            System.out.println("KACHUNK! "+b.name+" tipahti masiinasta!");
        }
    }

    public void returnMoney() {
        money = 0;
        System.out.println("Klink klink. Sinne menivät rahat!");
    }
}
