import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author n2863
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int Choice = 1;
        
        Bank bank = new Bank();
        
        while (Choice != 0){
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            Choice = scan.nextInt();
            
            switch(Choice){
                case 1:{
                    bank.addNormiTili();
                    break;
                }
                
                case 2:{
                    bank.addLuottoTili();
                    break;
                }
                
                case 3:{
                    bank.addMoney();
                    break;
                }
                
                case 4:{
                    bank.getMoney();
                    break;
                }
                
                case 5:{
                    bank.deleteTili();
                    break;
                }
                
                case 6:{
                    bank.printOne();
                    break;
                }
                
                case 7:{
                    bank.printAll();
                    break;
                }
                
                case 0:{
                    break;
                }
                
                default:{
                    System.out.println("Valinta ei kelpaa.");
                }
            }
        }
    }
    
}