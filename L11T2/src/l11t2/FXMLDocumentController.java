/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l11t2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private AnchorPane bg;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void clickAction(MouseEvent event) {
        Point p = new Point(event.getX(),event.getY());
    }

    EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>(){ 
        @Override 
        public void handle(MouseEvent e) { 
            System.out.println("Hei olen piste!"); 
        }
    };
       
    
    public class Point{
       private double xc;
       private double yc;
        

       public Point(double x,double y){
           xc = x;
           yc = y;
           Circle c = new Circle(x,y,7);
           bg.getChildren().add(c);
           c.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
            
       } 
      
    }
}
