/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l10t4;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {
    private String prev;
    private String next;
    private String now;
    
    private Label label;
    @FXML
    private TextField inputField;
    @FXML
    private Button searchButton;
    @FXML
    private WebView web;
    @FXML
    private Button refreshButton;
    @FXML
    private Button engButton;
    @FXML
    private Button fiButton;
    @FXML
    private Button previousButton;
    @FXML
    private Button nextButton;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void searchWeb(ActionEvent event) {
        try{
            prev = web.getEngine().getLocation();
        }
        catch (IllegalArgumentException ex){}
        
        if (inputField.getText().equals("index.html")){
            web.getEngine().load(getClass().getResource("index2.html").toExternalForm());
        }
        else{
            try{
            web.getEngine().load("https://"+inputField.getText());
            }
            catch (IllegalArgumentException ex){}
        }
    }

    @FXML
    private void refreshAction(ActionEvent event) {
        try{
            web.getEngine().load(web.getEngine().getLocation());
        }
        catch (IllegalArgumentException ex){}
    }

    @FXML
    private void engAction(ActionEvent event) {
        web.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void fiAction(ActionEvent event) {
        web.getEngine().executeScript("initialize()");
    }

    @FXML
    private void previousPage(ActionEvent event) {
        try{
            next = web.getEngine().getLocation();
            web.getEngine().load(prev);
            }
        catch (IllegalArgumentException ex){}
    }

    @FXML
    private void nextPage(ActionEvent event) {
        try{
            prev = web.getEngine().getLocation();
            web.getEngine().load(next);
            }
        catch (IllegalArgumentException ex){}
    }

    
}
