/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author n2863
 */
public class Dog {
        private String name;
        private String words;
        
    public Dog(){
        System.out.print("Anna koiralle nimi: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            name = br.readLine();
        } catch (IOException ex) {
        }
        
        if (name.trim().isEmpty()){
            name = "Doge";
        }
        

        System.out.println("Hei, nimeni on "+name+"!");

        System.out.print("Mitä koira sanoo: ");

        try {
            words = br.readLine();
        } catch (IOException ex) {
        }
        
        while (words.trim().isEmpty()){
            words = "Much wow!";
            
            System.out.println(name+": "+words);
            
            System.out.print("Mitä koira sanoo: ");
            try {
                words = br.readLine();
            } catch (IOException ex) {
            }
        } 
        System.out.println(name+": "+words);
    }
}
