/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l10t2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

/**
 *
 * @author n2863
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private TextField inputField;
    @FXML
    private Button searchButton;
    @FXML
    private WebView web;
    @FXML
    private Button refreshButton;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void searchWeb(ActionEvent event) {
        try{
        web.getEngine().load("https://"+inputField.getText());
        }
        catch (IllegalArgumentException ex){}
    }

    @FXML
    private void refreshAction(ActionEvent event) {
        try{
            web.getEngine().load(web.getEngine().getLocation());
        }
        catch (IllegalArgumentException ex){}
    }
    
}